package com.qwickeats;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.concurrent.TimeUnit;

import javax.swing.*;
 
@SuppressWarnings("serial")
public class DemoGrid extends JPanel {
	
	//Padding around the edges
    final int PAD = 20;
    final int ROWS = 25;
    final int COLS = 25;
	
	private static GridPoint[][] squares = null;
    private static DemoGrid demogrid = null;
	
	
    
    
    public DemoGrid() 
    {
    }
    
    public static DemoGrid getInstance( ) 
	{
    	if (demogrid == null)
    	{
    		 demogrid = new DemoGrid();
    	}
		return demogrid;
	}
    
    
    public static GridPoint[][] getSquares()
    {
    	if(squares == null)
    	{
    		squares = new GridPoint[25][25];
    	}
    	return squares;
    }
    
    public void pushDriverToSquare (int x, int y, Driver d)
    {
    	if (squares == null)
    	{
    		//System.out.println("Squares is null");
            initSquares();
        	squares[x][y].pushDriver(d);
    	} else
    	{
    		//System.out.println("Squares isn't null");
    		squares[x][y].pushDriver(d);
    	}
    }
    
    public void pushRestaurantToSquare (int x, int y, Restaurant r)
    {
    	if (squares == null)
    	{
    		//System.out.println("Squares is null");
            initSquares();
        	squares[x][y].setRestaurant(r);
    	} else
    	{
    		//System.out.println("Squares isn't null");
    		squares[x][y].setRestaurant(r);
    	}
    }
    
    
    
    private void initSquares() 
    {
       
        int w = getWidth();
        int h = getHeight();
        double xInc = (double)(w - 2*PAD)/COLS;
        double yInc = (double)(h - 2*PAD)/ROWS;
        
        for(int i = 0; i < ROWS; i++) 
        {
            double y = PAD + i*yInc;
            for(int j = 0; j < COLS; j++) 
            {
                double x = PAD + j*xInc;
                Rectangle2D.Double r =
                    new Rectangle2D.Double(x, y, xInc, yInc);
                GridPoint[][] squares_instance = getSquares();
                squares_instance[i][j] = new GridPoint(i, j, r);
            }
        }
        
        System.out.println(squares.length);
    }
  
    protected void paintComponent(Graphics g) {
    	System.out.println(javax.swing.SwingUtilities.isEventDispatchThread());

        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);

        
        if(squares == null) 
        {
            initSquares();
        }
        
        // Draw squares.
        g2.setPaint(Color.black);
        for(int i = 0; i < ROWS; i++) {
            for(int j = 0; j < COLS; j++) {
                squares[i][j].draw(g2);
            }
        }
    }
 
    protected void clearSquares()
    {
        for(int i = 0; i < ROWS; i++) {
        	for(int j = 0; j < COLS; j++) 
        	{
        		// clearDrivers
        		squares[i][j].clearDrivers();		
        	}
        }
    }
    
    
    
}