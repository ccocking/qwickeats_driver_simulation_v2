package com.qwickeats;

public class DriverDestinationDistance {
	
	// This tracks the index of the drivers in the driver pool
	private int driver_index;
	// This is the distance to the restaurant based on this drivers current_location
	private double distance_to_order_restaurant;
	
	public DriverDestinationDistance (Order o, Location driver_current_location, int new_driver_index)
	{
		setDistance_to_order_restaurant(Math.sqrt(Math.pow((o.getDestinationLocation().getX() - driver_current_location.getX()), 2) + Math.pow((o.getDestinationLocation().getY() - driver_current_location.getY()), 2)));
		setDriver_index(new_driver_index);
	}

	public int getDriver_index() {
		return driver_index;
	}

	public void setDriver_index(int driver_index) {
		this.driver_index = driver_index;
	}

	public double getDistance_to_order_restaurant() {
		return distance_to_order_restaurant;
	}

	public void setDistance_to_order_restaurant(double distance_to_order_restaurant) {
		this.distance_to_order_restaurant = distance_to_order_restaurant;
	}

}
