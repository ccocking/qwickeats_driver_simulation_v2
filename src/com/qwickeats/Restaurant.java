package com.qwickeats;

public class Restaurant 
{
	private String name;
	private Location location;
	private Order orders_received[];
	private Order orders_delivered[];
	
	Restaurant (String n, float lat, float lon, int x, int y)
	{
		location = new Location (lat, lon, x, y);
		name = n;
	}
	
	public void receive_order ()
	{
		
	}
	
	public void complete_order ()
	{
		
	}
	
	public Location getLocation()
	{
		return location;
	}

}
