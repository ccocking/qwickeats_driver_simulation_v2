package com.qwickeats;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;

public class Order 
{
	private Location destination;
	private Restaurant restaurant;
	private String order_details;
	private String order_received;
	
	
	
	Order (float d_lat, float d_lon, Restaurant r, String o_det ,int dx, int dy)
	{
		destination = new Location (d_lat, d_lon, dx, dy);
		restaurant = r;
		order_details = o_det;
		order_received = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());  
	}
	
	Order (String o_det, Restaurant r,int dx, int dy)
	{
		destination = new Location (dx, dy);
		restaurant = r;
		order_details = o_det;
		order_received = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()); 
		
	}
	
	
	
	public Restaurant getRestaurant() 
	{
		return this.restaurant;
	}
	
	public Location getDestinationLocation()
	{
		return this.destination;
	}
	
	public String getOrderDetails()
	{
		return this.order_details;
	}
	
	public String getOrderReceived()
	{
		return this.order_received;
	}

}
