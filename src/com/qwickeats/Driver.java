package com.qwickeats;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class Driver 
{
	private int identifier;
	private int order_carry_count;
	private int daily_deliveries;
	private int total_deliveries;
	private int delivery_zone;
	private Location current_location;
	private Location immediate_destination;
    private Location base;
	private LinkedList<Order> queued_orders;
	// TODO Change the deliver_destinations priority generic Orders - this will allow me to cleanly add completed orders the completed_orders queue
	private LinkedList<Order> completed_orders;
	private PriorityQueue<Restaurant> restaurant_destinations;
	private PriorityQueue<Location> delivery_destinations;
	private State state;
	private Timer buffer_timer;
	private Timer current_trip_timer;
	
	private  class RestaurantComparator implements Comparator<Restaurant>
	{
		 @Override
		 public int compare(Restaurant x, Restaurant y)
		 {

		     if (x.getLocation().distanceToLocation(current_location) < y.getLocation().distanceToLocation(current_location))
		     {
		         return 1;
		     }
		     if (x.getLocation().distanceToLocation(current_location) > y.getLocation().distanceToLocation(current_location))
		     {
		         return -1;
		     }
		     return 0;
		 }	 
	}
	
	private  class DestinationComparator implements Comparator<Location>
	{
		 @Override
		 public int compare(Location x, Location y)
		 {

		     if (x.distanceToLocation(current_location) < y.distanceToLocation(current_location))
		     {
		         return 1;
		     }
		     if (x.distanceToLocation(current_location) > y.distanceToLocation(current_location))
		     {
		         return -1;
		     }
		     return 0;
		 }	 
	}
	
	public class MyTimerTask extends TimerTask {
		 
	    // All this does is changes the state of the driver after a certain amount of time
	    public void run() {
	    	if (state == State.EN_ROUTE_RESTAURANT_AVAILABLE)
	    	{
	    	changeState(State.EN_ROUTE_RESTAURANT_UNAVAILABLE);
	    	}
	    }
	}
	
	
	
	// Constructor to initialize the driver to a location
	Driver (float latInitial, float lonInitial, int x, int y, int new_identifier)
	{
        Comparator<Restaurant> comparator = new RestaurantComparator();
        Comparator<Location> comparator2 = new DestinationComparator();

        identifier = new_identifier;
		restaurant_destinations = new PriorityQueue<Restaurant>(10, comparator);
		delivery_destinations = new PriorityQueue<Location>(10, comparator2);
		queued_orders = new LinkedList<Order>();
		current_location = new Location (latInitial,lonInitial, x ,y);
        base = new Location (latInitial,lonInitial, x ,y) ;
		immediate_destination = new Location(latInitial,lonInitial, 5,5);
		state = State.AVAILABLE;
		order_carry_count = 0;
		buffer_timer = new Timer ();
		current_trip_timer = new Timer ();
		delivery_zone = 0;
	}
	
	public LinkedList<Order> getActiveOrders()
	{
		return this.queued_orders;
	}
	
	public Location getImmediateDestination()
	{
		return this.immediate_destination;
	}
	
	// Method to change the state of a driver
	private void changeState (State new_state)
	{
		this.state = new_state;
	}
	
	public Location getLocation ()
	{
		return this.current_location;
	}
	
	// Method to increment the carry count of a driver
	public int increment_carry_count ()
	{
		this.order_carry_count++;
		return order_carry_count;
	}
	// Method to decrement the carry count of a driver
	public int decrement_carry_count ()
	{
		this.order_carry_count--;
		return order_carry_count;
	}
	
	public void updateState()
	{
		
	}
	
	public State getState()
	{
		return this.state;
	}
	
	// Adds an order to the Driver's order queue
	public boolean assignOrder(Order o)
	{
		boolean assign_success =  false;
		switch (this.state)
		{
			case AVAILABLE:
				// Driver needs to add the restaurant to his priority queue for restaurants
				this.restaurant_destinations.add(o.getRestaurant());
				// Driver adds the order to his queued orders
				this.queued_orders.add(o);
				// Change state to en_route_restaurant_availabe
				this.changeState(State.EN_ROUTE_RESTAURANT_AVAILABLE);
				TimerTask timerTask = new MyTimerTask();
			    //running timer task as daemon thread
			    buffer_timer = new Timer(true);
			    buffer_timer.schedule(timerTask, 10*1000);
				// Fire off the timer to begin counting down to where this driver takes no more orders
				this.setImmediateDestination(this.restaurant_destinations.peek().getLocation());
                assign_success =  true;
				break;
			case EN_ROUTE_RESTAURANT_AVAILABLE:
				// Driver adds the restaurant to his priority queue for restaurants
				this.restaurant_destinations.add(o.getRestaurant());
				// Driver adds the order his queued orders
				this.queued_orders.add(o);
				// TODO Determine the immediate destination given this new addition
                this.setImmediateDestination(this.restaurant_destinations.peek().getLocation());

				assign_success =  true;
				break;
			case EN_ROUTE_DESTINATION:
				// Driver adds the restaurant to his priority queue for restaurants
				this.restaurant_destinations.add(o.getRestaurant());
				// Driver adds the order his queued orders
				this.queued_orders.add(o);
				// TODO Determine the immediate destination given this new addition
				assign_success =  true;
				break;
			case EN_ROUTE_RESTAURANT_UNAVAILABLE:
				assign_success =  false;
				break;
		}
		this.moveTowardsImmediateDestination();
		return assign_success;
		
		// This will change the state to en_route_unavailable
		//		this.buffer_timer = new Timer(3000, new ActionListener() {
		//			@Override
		//			public void actionPerformed(ActionEvent arg0) {
		//				
		//			}
		//			});
		//		this.buffer_timer.setRepeats(false); // Only execute once
		//		this.buffer_timer.start();
	}
	// Pops an order from the Driver's order queue and saves to the database
	
	public void progressDeliveryPhase()
	{
        boolean do_nothing_flag = false;

        // This method handles state and destination once the driver arrives to his immediate location
		switch (this.state)
		{
		case AVAILABLE:
			// This would mean you reached the destination in the random walk case -> movethedriver
            do_nothing_flag = true;
            break;
		case EN_ROUTE_DESTINATION:
			// This means you have arrived at your destination for delivery. Need to check if you have anymore orders to deliver.
			// If so, then progress towards towards your new destination without changing state
			Location current_location_destination = this.delivery_destinations.poll();
			
			if (current_location_destination == null)
			{
				System.out.println("This should never happen");
			} else
			{
				// Peak the current destinations queue and see if there are any left
				Location next_destination = this.delivery_destinations.peek();
				if (next_destination ==  null)
				{
					// Check if we need to start going to restaurants
					Restaurant potential_restaurant_location = this.restaurant_destinations.poll();
					if (potential_restaurant_location == null)
					{
						// update state to AVAILABLE AND begin "wondering phase"
						this.changeState(State.AVAILABLE);
					} else
					{
						//TODO update your current destination to this restaurant and change your state to EN_ROUTE_AVAIALABLE + Fire Timer
						this.changeState(State.EN_ROUTE_RESTAURANT_AVAILABLE);
						TimerTask timerTask = new MyTimerTask();
					    //running timer task as daemon thread
					    buffer_timer = new Timer(true);
					    buffer_timer.schedule(timerTask, 10*1000); 
						this.setImmediateDestination(potential_restaurant_location.getLocation());
					}
					
				} else
				{	// You have more deliveries continue to the next one
					this.setImmediateDestination(next_destination);
				}
			}
			
			
			break;
		case EN_ROUTE_RESTAURANT_AVAILABLE:
			// Pop the current destination off the restaurant queue
			Restaurant removed_item = this.restaurant_destinations.poll();
			//
			if (removed_item == null)
			{
				// 2 cases -> YOu have restaurants in your queue or you don't. The idea here is that if the driver has arrived at the restaurant and needs to way for his
				// buffer timer to finish before beginning the delivery phase of his journey. The timer finishing implies that his state will change
				// You should check if there are other restaurants in your queue though. 			

				// There are no more restaurants left for the driver to go to - transition to the delivery phase
				
				// Update the state to reflect that the driver is en_route to their destination
				this.changeState(State.EN_ROUTE_DESTINATION);
				
				// Add each Location from the queued orders to the destination priority queue
				ListIterator<Order> listIterator = queued_orders.listIterator();
				while (listIterator.hasNext())
				{	
					// Add the Order Location to the delivery_destination priority queue
					this.delivery_destinations.add(listIterator.next().getDestinationLocation());
				}
				// Clear the queue of the previous orders
				this.queued_orders.clear();
				this.setImmediateDestination(this.delivery_destinations.peek());	
			} else
			{ 
				// Literally do nothing..wait for your timer to finish and then you will hit the en_route_restaurant_unavailable and begin your delivery phase
			}
			break;
		case EN_ROUTE_RESTAURANT_UNAVAILABLE:
			// Pop the current destination off the restaurant queue
			Restaurant removed_item_2 = this.restaurant_destinations.poll();
			//
			if (removed_item_2 == null)
			{
				// There are no more restaurants left for the driver to go to - transition to the delivery phase
				
				// Update the state to reflect that the driver is en_route to their destination
				this.changeState(State.EN_ROUTE_DESTINATION);
				
				// Add each Location from the queued orders to the destination priority queue
				ListIterator<Order> listIterator = queued_orders.listIterator();
				while (listIterator.hasNext())
				{	
					// Add the Order Location to the delivery_destination priority queue
					this.delivery_destinations.add(listIterator.next().getDestinationLocation());
				}
				//Clear the queue
				this.queued_orders.clear();
				this.setImmediateDestination(this.delivery_destinations.peek());	
			} else
			{ 
				Restaurant next_destination_only = this.restaurant_destinations.peek();
				if (next_destination_only ==  null)
				{
					// Update the state to reflect that the driver is en_route to their destination
					this.changeState(State.EN_ROUTE_DESTINATION);
					
					// Add each Location from the queued orders to the destination priority queue
					ListIterator<Order> listIterator = queued_orders.listIterator();
					while (listIterator.hasNext())
					{	
						// Add the Order Location to the delivery_destination priority queue
						this.delivery_destinations.add(listIterator.next().getDestinationLocation());
					}
					//Clear the queue
					this.queued_orders.clear();
					this.setImmediateDestination(this.delivery_destinations.peek());

				} else
				{	// You have more deliveries continue to the next one
					this.setImmediateDestination(next_destination_only.getLocation());
				}
			}
			
			break;

		}
        if (!do_nothing_flag) {
            moveTowardsImmediateDestination();
        }
        }
	
	public void completeOrder()
	{
		
	}
	//
	public void cancelOrder()
	{
		
	}
	
	public void setImmediateDestination(Location x)
	{
		this.immediate_destination = x;
	}

    public int getIdentifier()
    {
        return identifier;
    }

	public void moveTowardsImmediateDestination()
	{	

		System.out.printf("%n Driver %d : Located at %d-%d with state %s %n"
				+ "Restaurant Destination Queue: %s. %n"
				+ "Queued Destinations: %s. %n"
				+ "Delivery Destination Queue: %s %n"
				+ "Immediate Destination %d %d %n", this.identifier,this.current_location.getX(), this.current_location.getY(),this.state, this.restaurant_destinations.toString(),this.queued_orders.toString(),this.delivery_destinations.toString(),this.immediate_destination.getX(),this.immediate_destination.getY());;
		
		switch (this.state)
		{
		case AVAILABLE:
            // When you are available always move towards your base
			this.setImmediateDestination(base);
			break;
		case EN_ROUTE_DESTINATION:
			// Deal with re_route once you have completed an order - always continue to current location when en_route to a destination
			break;
		case EN_ROUTE_RESTAURANT_AVAILABLE:
			// Route to a new restaurant 
			break;
		case EN_ROUTE_RESTAURANT_UNAVAILABLE:
			// Set theImmediateDestination to the immediate restaurant destination
			break;
		}
		
		
		
		// Move the driver by one spot
		if (this.current_location.getX() < this.immediate_destination.getX())
		{	
			// Move to the right
			this.current_location.setX(this.current_location.getX() + 1);
		} else if (this.current_location.getX() > this.immediate_destination.getX())
		{
			// Move to the left
			this.current_location.setX(this.current_location.getX() - 1);
		} else if (this.current_location.getY() < this.immediate_destination.getY())
		{
			// Move upwards
			this.current_location.setY(this.current_location.getY() + 1);
		} else if (this.current_location.getY() > this.immediate_destination.getY())
		{
			// Move downwards
			this.current_location.setY(this.current_location.getY() - 1);
		}
		// Check if you have arrived
		if (this.current_location.compareLocationsXY(immediate_destination))
		{
			this.progressDeliveryPhase();
			//Assign a new immediate destination because you have arrived
		}
	
		
	}

	
	//Simulates the driving by moving on step towards the current destination
}
