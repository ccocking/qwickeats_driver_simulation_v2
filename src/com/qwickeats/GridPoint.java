package com.qwickeats;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;

public class GridPoint 
{
    private final int row;
    private final int col;
    private boolean occupied = false;
    Color square_color = Color.black ;
    
    Color no_driver_no_restaurant = Color.white;
    Color restaurant_only = Color.red;
    Color rest_plus_drivers = Color.green;
    
    Color one_driver = Color.blue;
    Color two_drivers = Color.MAGENTA;
    Color more_than_two_drivers = Color.green;
    Rectangle2D.Double rect;
    private LinkedList<Driver> drivers;
    private Restaurant static_restaurant;
    private boolean selected = false;
    
    
    public GridPoint(int r, int c, Rectangle2D.Double rect) {
    	drivers = new LinkedList<Driver>();
    	static_restaurant = null;
    	this.row = r;
        this.col = c;
        this.rect = rect;
    }
    
    public GridPoint(int r, int c, Rectangle2D.Double rect, Restaurant res) {
        static_restaurant = res;
    	this.row = r;
        this.col = c;
        this.rect = rect;
    }
    
    public void pushDriver (Driver d)
    {
    	drivers.add(d);  

    }
   
    public void setRestaurant (Restaurant d)
    {
    	static_restaurant = d;
    }
    public void draw(Graphics2D g2) 
    {
    	
    	if (drivers != null)
    	{
    		if (static_restaurant == null)
    		{
		    	if (drivers.size() == 0) 
		    	{
		    		g2.setPaint(no_driver_no_restaurant);
		    	} else if (drivers.size() == 1)
		    	{
		    		g2.setPaint(one_driver);
		    	} else if (drivers.size() == 2)
		    	{
		    		g2.setPaint(two_drivers);
		    	} else if (drivers.size() > 2)
		    	{
		    		g2.setPaint(more_than_two_drivers);
		    	}
    		} else
    		{
    			if (drivers.size() == 0) 
		    	{
		    		g2.setPaint(restaurant_only);
		    	} else 
		    	{
		    		g2.setPaint(rest_plus_drivers);
		    	}
    		}
    	} else
    	{
    		if (static_restaurant ==null)
    		{
    			g2.setPaint(no_driver_no_restaurant);
    		} else
    		{
        		g2.setPaint(restaurant_only);
    		}
    	}
        g2.fill(rect);
        g2.setPaint(square_color);
        g2.draw(rect);
    }
    
    public void clearDrivers()
    {
    	this.drivers.clear();
    }
 
    public void setOccupied(boolean occupied) 
    {
        this.occupied = occupied;
    }
 
    public boolean isOccupied() 
    {
        return occupied;
    }
}