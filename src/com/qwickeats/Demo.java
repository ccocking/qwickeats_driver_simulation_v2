package com.qwickeats;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

public class Demo implements KeyListener {

	private static boolean wPressed = false;
	private static Dispatcher dispatcher = null;
 	public static boolean isWPressed() {
	        synchronized (Demo.class) {
	            return wPressed;
	        }
	 }
	

	
	
	public static void main(String[] args) throws IOException 
	{	
		

		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {

            public boolean dispatchKeyEvent(KeyEvent ke) {
                synchronized (Demo.class) {
                    switch (ke.getID()) {
                    case KeyEvent.KEY_PRESSED:
                        if (ke.getKeyCode() == KeyEvent.VK_W) {
                            wPressed = true;
                        }
                        break;

                    case KeyEvent.KEY_RELEASED:
                        if (ke.getKeyCode() == KeyEvent.VK_W) {
                            wPressed = false;
                        }
                        break;
                    }
                    return false;
                }
            }
        });

	    Random randomGenerator = new Random();
	    LinkedList<Restaurant>  restaurant_list= new LinkedList<Restaurant> ();
		// Create the dispatcher
		dispatcher = Dispatcher.getInstance();
		ArrayList<Integer> driver_coords = new ArrayList<Integer>();
		//Create 3 Random Drivers and push to the dispatchers queue
		System.out.println("Generating Drivers");
		for (int i = 0; i < 5 ; i++)
		{
			float lat = (float) (randomGenerator.nextFloat() * (18.070000 - 17.900000) + 18.070000);
			float lon = (float) (randomGenerator.nextFloat() * (76.870000 - 76.720000) + 76.870000) * -1;
			int x = randomGenerator.nextInt(24);
			int y = randomGenerator.nextInt(24);
			driver_coords.add(x);
			driver_coords.add(y);
			System.out.print(x);
			System.out.print(",");
			System.out.println(y);
			Driver new_driver = new Driver (lat,lon,x,y, i+1);
			dispatcher.addDriver(new_driver);
			//test.pushDriverToSquare(x, y, new_driver);
		}
		System.out.println(driver_coords.toString());
		writeDriverLocationToFile(driver_coords);

		
		String restaurant_names[] = new String[] {"JoJos JerkPit","KFC New Kingston", "KFC Mona", "Little Tokyo","Burger King Liguanea"};
		ArrayList<Integer> rest_coords = new ArrayList<Integer>();
		System.out.println("Restaurants");
		// Create 5 Random Restaurants and push to the Restaurants queue
		for (int i = 0; i < 5 ; i++)
		{
			float lat = (float) (randomGenerator.nextFloat() * (18.070000 - 17.900000) + 18.070000);
			float lon = (float) (randomGenerator.nextFloat() * (76.870000 - 76.720000) + 76.870000) * -1;
			int x = randomGenerator.nextInt(24);
			int y = randomGenerator.nextInt(24);
			
			Restaurant new_restaurant =  new Restaurant (restaurant_names[i], lat, lon, x, y);
			rest_coords.add(x);
			rest_coords.add(y);
			System.out.print(x);
			System.out.print(",");
			System.out.println(y);
			//test.pushRestaurantToSquare(x, y, new_restaurant);
			restaurant_list.add(new_restaurant);
		}
		System.out.println(rest_coords.toString());
		writeRestaurantLocationToFile(rest_coords);
		
		while (true)
		{ 
			while (true)
			{
				 try 
				 {
					 TimeUnit.SECONDS.sleep(1);
				 } catch (InterruptedException e) 
				 {
					 // TODO Auto-generated catch block
					 e.printStackTrace();
				 }
				 
				 // Random Order Generator + Dispatch
				 
				int generate_order = randomGenerator.nextInt(100);
				
				if (generate_order > 75)
				{	// Create the random order
					
					// Choose the restaurant 
					int restaurant_choice_index = randomGenerator.nextInt(restaurant_list.size());
					Restaurant restaurant_choice = restaurant_list.get(restaurant_choice_index);						
					String order_details = "Fake Randomly generated order";
					
					// Generate the destination
					int destination_x = randomGenerator.nextInt(24);
					int destination_y = randomGenerator.nextInt(24);
					int check_dest = 1;
					while (check_dest == 1)
					{
						check_dest = 0;
						if (destination_x == restaurant_choice.getLocation().getX() && destination_y == restaurant_choice.getLocation().getY())
						{
							// Regenerate the destination if it happens to be the same as the restaurant
							destination_x = randomGenerator.nextInt(24);
							destination_y = randomGenerator.nextInt(24);
							check_dest = 1;
						}
					}
					
					Order new_order = new Order(order_details, restaurant_choice,destination_x,destination_y);
					System.out.println("Order Generated");
					// Call the algorithm and dispatch the order
					
					boolean dispatch_success = dispatcher.dispatchOrder(new_order);
					if (dispatch_success)
					{
						System.out.println("Order Dispatched");
					}else
					{
						System.out.println("Could not dispatch order");
					}
					

				}
				writeDestinationLocationToFile();

				//Clear the clears of the current drivers
				//test.clearSquares();
				LinkedList<Driver> driver_pool = dispatcher.getDriverPool();
				ArrayList<Integer> driver_coords1 = new ArrayList<Integer>();
				ListIterator<Driver> listIterator = driver_pool.listIterator();
				dispatcher.moveDrivers();
				while (listIterator.hasNext())
				{
					Driver current_driver = listIterator.next();
					driver_coords1.add(current_driver.getLocation().getX());
					driver_coords1.add(current_driver.getLocation().getY());
					//test.pushDriverToSquare(current_driver.getLocation().getX(), current_driver.getLocation().getY(), current_driver);
				}
				writeDriverLocationToFile(driver_coords1);

			}
		}
	}
	
	public static void writeDriverLocationToFile (ArrayList<Integer> coords) throws IOException 
	{
		File file = new File("/Users/christiancocking/Developer/eclipse_workspace/QwickeatsDriverSimulation_v2/qwickeats_demo/drivers.csv");
		file.delete();
		FileWriter fwriter = new FileWriter("/Users/christiancocking/Developer/eclipse_workspace/QwickeatsDriverSimulation_v2/qwickeats_demo/drivers.csv", true);
		PrintWriter print_line = new PrintWriter( fwriter );
		print_line.printf("Y,X,%n");
		for (int i = 0; i < coords.size(); i = i+2)
		{
			print_line.printf("%d,%d,%n",coords.get(i),coords.get(i+1));
		}
		print_line.close();
	}
	
	public static void writeRestaurantLocationToFile (ArrayList<Integer> coords) throws IOException 
	{
		File file = new File("/Users/christiancocking/Developer/eclipse_workspace/QwickeatsDriverSimulation_v2/qwickeats_demo/restaurants.csv");
		file.delete();
		FileWriter fwriter = new FileWriter("/Users/christiancocking/Developer/eclipse_workspace/QwickeatsDriverSimulation_v2/qwickeats_demo/restaurants.csv", true);
		PrintWriter print_line = new PrintWriter( fwriter );
		print_line.printf("Y,X, %n");
		for (int i = 0; i < coords.size(); i= i+2)
		{	
			print_line.printf("%d,%d, %n",coords.get(i),coords.get(i+1));
		}
		print_line.close();
	}
	
	public static void writeDestinationLocationToFile () throws IOException
	{	
		File file = new File("/Users/christiancocking/Developer/eclipse_workspace/QwickeatsDriverSimulation_v2/qwickeats_demo/destinations.csv");
		file.delete();
		FileWriter fwriter = new FileWriter("/Users/christiancocking/Developer/eclipse_workspace/QwickeatsDriverSimulation_v2/qwickeats_demo/destinations.csv", true);
		PrintWriter print_line = new PrintWriter( fwriter );
		print_line.printf("Y,X, %n");
		LinkedList<Driver> current_drivers =  dispatcher.getDriverPool();
		for(int i = 0; i < current_drivers.size(); i++)
		{
			// Lets get the immediate destination of each driver on this
			print_line.printf("%d,%d, %n",current_drivers.get(i).getImmediateDestination().getX(),current_drivers.get(i).getImmediateDestination().getY());
			
//			// Get the active orders for each driver
//			ListIterator<Order> orderIterator = current_drivers.get(i).getActiveOrders().listIterator();
//			while (orderIterator.hasNext())
//			{ 	// Iterating through each order writing the destination to a file for Processing to consume
//				Order current_order = orderIterator.next();
//				Location order_destination = current_order.getDestinationLocation();
//				print_line.printf("%d,%d, %n",order_destination.getX(),order_destination.getY());	
//			}
		}
		print_line.close();		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println(arg0);
	}



	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println(arg0);
	}



	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println(arg0);
	}
}
