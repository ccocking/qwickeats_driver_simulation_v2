package com.qwickeats;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.PriorityQueue;

public class Dispatcher {

	
	private static Dispatcher dispatcher = new Dispatcher();
	private LinkedList<Driver> driver_pool;
	private PriorityQueue<DriverDestinationDistance> decision_maker;
	
	private  class DriverDestinationDistanceComparator implements Comparator<DriverDestinationDistance>
	{
		 public int compare(DriverDestinationDistance x, DriverDestinationDistance y)
		 {

		     if (x.getDistance_to_order_restaurant() < y.getDistance_to_order_restaurant())
		     {
		         return -1;
		     }
		     if (x.getDistance_to_order_restaurant() > y.getDistance_to_order_restaurant())
		     {
		         return 1;
		     }
		     return 0;
		 }	 
	}
	
	
	
	
	
	/* A private Constructor prevents any other 
	 * class from instantiating.
	 */
	private Dispatcher()
	{
		if (driver_pool == null)
		{
			driver_pool = new LinkedList<Driver> ();
		}
		DriverDestinationDistanceComparator comp = new DriverDestinationDistanceComparator();
		decision_maker = new PriorityQueue<DriverDestinationDistance>(20, comp);

	}
	
	public static Dispatcher getInstance( ) 
	{
		return dispatcher;
	}
	/** 
	 * This method is the algorithm, given an order and the current driver pool it will
	 * dispatch the order to the driver who should ideally get the order
	 * 
	 **/
	protected boolean dispatchOrder(Order o)
	{
		// Loop through the driver pool
		for (int i = 0; i < driver_pool.size(); i++)
		{
			// For now lets ignore drivers that are giving a delivery
	
			if (driver_pool.get(i).getState() == State.EN_ROUTE_RESTAURANT_UNAVAILABLE  || driver_pool.get(i).getState() == State.EN_ROUTE_DESTINATION)
			{
				continue;
			} else
			{
				DriverDestinationDistance add_to_queue = new DriverDestinationDistance(o, driver_pool.get(i).getLocation(), i);
				System.out.printf("Driver %d is %f from the order %n", driver_pool.get(i).getIdentifier(),add_to_queue.getDistance_to_order_restaurant());
				// This creates a priority queue with the topmost member being the
				this.decision_maker.add(add_to_queue);
			}
		}
		// decision_maker now has the drivers that are closest to the order, it is possible for decision_maker to be empty
		System.out.println("Decision Making PQueue");
		System.out.println(decision_maker.toString());
		
		DriverDestinationDistance selected_driver_calculation = decision_maker.poll();
		// Loop until we find a driver
		while (selected_driver_calculation == null)
		{
		// Open up the search to include those drivers currently doing a delivery
		for (int i = 0; i < driver_pool.size(); i++)
		{
			// Cannot route to drivers that are unavailable - they need to start delivering foo'
			if (driver_pool.get(i).getState() == State.EN_ROUTE_RESTAURANT_UNAVAILABLE )
			{
				continue;
			} else
			{
				// Based on their immediate immediate
				DriverDestinationDistance add_to_queue = new DriverDestinationDistance(o, driver_pool.get(i).getLocation(), i);
				// This creates a priority queue with the topmost member being the 
				this.decision_maker.add(add_to_queue);
			}
		}
		
		 selected_driver_calculation = decision_maker.poll();
		}
		
		
		// We have our driver now.. dispatch the order
		
		// TODO need to null check here maybe there is an issue
		Driver selected_driver = this.getDriverPool().get(selected_driver_calculation.getDriver_index());
		
		boolean assingment_success = selected_driver.assignOrder(o);
		
		if (assingment_success)
		{
			return true;
		} else
		{
			return false;
		}
	
		
		// Determine the delivery zone of the order (by destination or by restaurant (Me thinks RESTAURANT)? this portion can be worked into the app)-> if so give the order to the
		
		// Run a check on all drivers with an acceptable buffer time going to that restaurant (or near to that restaurant)
		
		
		// If the above check does not work route the driver that is closet to the restaurant with no available orders
		
		
		// If none of the above drivers are available check drivers closest to their destination with the least remaining orders
		
		
		//
		
		
	}
	
	
	protected int determineDeliveryZone (Location l)
	{
		return 0;
	}
	
	
	protected void moveDrivers()
	{
		ListIterator<Driver> listIterator = this.driver_pool.listIterator();
		while (listIterator.hasNext())
		{
			Driver current_driver = listIterator.next();
			current_driver.moveTowardsImmediateDestination();
		}
	}
	
	protected void addDriver(Driver d)
	{
		this.driver_pool.add(d);
	}

	protected LinkedList<Driver> getDriverPool()
	{
		return this.driver_pool;
	}

}