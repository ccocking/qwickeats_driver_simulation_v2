package com.qwickeats;

// Driver's location at any given time
public class Location 
{
	private float latitude;
	private float longitude;
	private int x_position;
	private int y_position;
	// Initialize the location object for each driver
	Location (float latInitial, float lonInitial, int x_pos, int y_pos)
	{
		latitude = latInitial;
		longitude = lonInitial;
		x_position = x_pos;
		y_position = y_pos;
	}
	
	Location ( int x_pos, int y_pos)
	{
		x_position = x_pos;
		y_position = y_pos;
	}
	
	
	public float getLat()
	{
		return this.latitude;
	}
	
	public float getLon()
	{
		return this.longitude;
	}
	
	public int getX()
	{
		return this.x_position;
	}
	
	public int getY()
	{
		return this.y_position;
	}
	
	public void setY (int y)
	{
		this.y_position = y;
	}
	
	public void setX(int x)
	{
		this.x_position = x;
	}
	
	public boolean compareLocationsXY (Location other_location)
	{
		if ((this.x_position == other_location.getX()) && (this.y_position == other_location.getY()) )
		{
			return true;
		} else
		{
			return false;
		}
	}
	
	public double distanceToLocation (Location l)
	{
		return Math.sqrt(Math.pow((l.getX() - this.getX()), 2) + Math.pow((l.getY() - this.getY()), 2));

	}
}
