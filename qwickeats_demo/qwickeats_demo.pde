
// 2D Array of objects
Cell[][] grid;

// Number of columns and rows in the grid
int cols = 25;
int rows = 25;

void setup() {
  frameRate(1);
  size(500,500);
  grid = new Cell[cols][rows];
  for (int i = 0; i < cols; i++) {
    for (int j = 0; j < rows; j++) {
      // Initialize each object
      grid[i][j] = new Cell(i*20,j*20,20,20,i+j);
    }
  }
  Table table = loadTable("restaurants.csv", "header");
  
  for (TableRow row : table.rows())
  {
    int x = row.getInt("X");            
    int y = row.getInt("Y"); 
    println(row);
    println(x);
    println(y);
    grid[x][y].incr_rest_count();
  }
  
  
  Table table2 = loadTable("drivers.csv", "header");
  
  for (TableRow row : table2.rows())
  {
    int x = row.getInt("X");            
    int y = row.getInt("Y"); 
    println(row);
    println(x);
    println(y);
    grid[x][y].incr_driver_count();
  }
  
  Table table3 = loadTable("destinations.csv", "header");
  
  for (TableRow row : table3.rows())
  {
    int x = row.getInt("X");            
    int y = row.getInt("Y"); 
    println(row);
    println(x);
    println(y);
    grid[x][y].incr_dest_count();
  }
  
  
}

void draw() {
  background(0);
  
   for (int i = 0; i < cols; i++) {
    for (int j = 0; j < rows; j++) {
      // Initialize each object
      grid[i][j].clear_squares();
    }
  }

  Table table = loadTable("restaurants.csv", "header");
  
  for (TableRow row : table.rows())
  {
    int x = row.getInt("X");            
    int y = row.getInt("Y"); 
    println(row);
    println(x);
    println(y);
    grid[x][y].incr_rest_count();
  }
  
  
  Table table2 = loadTable("drivers.csv", "header");
  
  for (TableRow row : table2.rows())
  {
    int x = row.getInt("X");            
    int y = row.getInt("Y"); 
    println(row);
    println(x);
    println(y);
    grid[x][y].incr_driver_count();
  }
  
  Table table3 = loadTable("destinations.csv", "header");
  
  for (TableRow row : table3.rows())
  {
    int x = row.getInt("X");            
    int y = row.getInt("Y"); 
    println(row);
    println(x);
    println(y);
    grid[x][y].incr_dest_count();
  }
  
    // The counter variables i and j are also the column and row numbers and 
  // are used as arguments to the constructor for each object in the grid.  
  for (int i = 0; i < cols; i++) {
    for (int j = 0; j < rows; j++) {
      // Oscillate and display each object
      grid[i][j].display();
    }
  }
  
  

}

// A Cell object
class Cell {
  // A cell object knows about its location in the grid as well as its size with the variables x,y,w,h.
  float x,y;   // x,y location
  int driver_count;
  int restaurant_count;
  int destination_count;
  int base_count;
  float w,h;   // width and height

  // Cell Constructor
  Cell(float tempX, float tempY, float tempW, float tempH, float tempAngle) 
  {
    x = tempX;
    y = tempY;
    w = tempW;
    h = tempH;
  }
 
  void clear_squares()
  {
   driver_count = 0;
   restaurant_count = 0;
   destination_count = 0;
  } 
  
  void incr_driver_count()
  {
    driver_count++;
  }
  
  void incr_rest_count()
  {
    restaurant_count++;
  }
  void incr_dest_count()
  {
    destination_count++;
  }
  void display() {
    stroke(0);
    boolean render_non_base_square = true;
    if (destination_count == 1 && driver_count == 1 && restaurant_count == 0)
    {
        fill(0,0,0);
      // At home base or at successful delivery
       render_non_base_square = false;  
    }
    
    
    if (render_non_base_square){
      if (destination_count == 0)
      {
        if (driver_count == 0 && restaurant_count == 0)
        {
          fill(255,255,255);
        } else if (driver_count > 0 && restaurant_count == 0)
        { // Drivers and No restaurant
          fill (250,15,15);
        } else if (driver_count > 0 && restaurant_count > 0) 
        { // 
          fill(250,246,16);  
        } else if (driver_count == 0 && restaurant_count > 0)
        {
          fill(15,15,250);
        }
      } else
      {
        if (restaurant_count > 0 )
        {
          fill(179,238,255);
        }else if (destination_count == 1)
        {
          fill(41,172,85);
        } else
        {
         fill(199,113,209); 
        }
      }
    }
    rect(x,y,w,h); 
}
}

